<?php

use Illuminate\Support\Arr;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;

/**
 * @author [prasanna]
 * @email [info.prasannam@gmail.com]
 * @modify date 2022-03-14 13:13:53
 */

/**
 * Return json response.
 *
 * @param array $responseData
 * @return @return \Illuminate\Http\Response
 */
function jsonErrorResponse($responseData = null)
{
    $request = new \stdClass;
    $code = null;
    $statusCode = null;
    $data = array();
    $plain = false;

    if (isset($responseData->request)) {
        $request = $responseData->request;
    }
    if (isset($responseData->code)) {
        $code = $responseData->code;
    }
    if (isset($responseData->statusCode)) {
        $statusCode = $responseData->statusCode;
    }
    if (isset($responseData->data)) {
        $data = $responseData->data;
    }
    if (isset($responseData->plain)) {
        $plain = $responseData->plain;
    }

    if(!Arr::accessible($data)) {
        $data = Arr::wrap($data);
    }

    if (!isset($statusCode)) {
        return response()->json(['statusCode' => Response::HTTP_NOT_FOUND, 'error' => 'Error', 'message' => 'Oh noes, something went wrong'], Response::HTTP_NOT_FOUND);
    }

    if ($plain) {
        return response()->json($data, $statusCode);
    }

    if (isset($code)) {
        if ($request->hasHeader('accept-language') && in_array($request->header('accept-language'), ['en', 'si', 'ta'])) {
            App::setLocale($request->header('accept-language'));
        }

        $message = '';
        if ('C' . $code != __('C' . $code)) {
            $message = __('C' . $code);
        }

        $response = [
            'statusCode' => $statusCode,
            'message' => $message,
            'code' => $code
        ];

        if (!empty($data)) {
            $response['data'] = $data;
        }

        return response()->json($response, $statusCode);
    }

    return response()->json($data, $statusCode);
}

/**
 * Return json response.
 *
 * @param array $responseData
 * @return @return \Illuminate\Http\Response
 */
function jsonResponse($responseData = null)
{
    $request = new \stdClass;
    $code = null;
    $statusCode = Response::HTTP_OK;
    $data = array();
    $plain = false;

    if (isset($responseData->request)) {
        $request = $responseData->request;
    }
    if (isset($responseData->code)) {
        $code = $responseData->code;
    }
    if (isset($responseData->statusCode)) {
        $statusCode = $responseData->statusCode;
    }
    if (isset($responseData->data)) {
        $data = $responseData->data;
    }
    if (isset($responseData->plain)) {
        $plain = $responseData->plain;
    }

    if(!Arr::accessible($data)) {
        $data = Arr::wrap($data);
    }

    if ($plain && !$request->hasHeader('full-response')) {
        return response()->json($data, $statusCode);
    }

    if (isset($code) && $request->hasHeader('full-response')) {
        if ($request->hasHeader('accept-language') && in_array($request->header('accept-language'), ['en', 'si', 'ta'])) {
            App::setLocale($request->header('accept-language'));
        }

        $message = '';
        if ('C' . $code != __('C' . $code)) {
            $message = __('C' . $code);
        }

        $response = [
            'statusCode' => $statusCode,
            'message' => $message,
            'code' => $code
        ];

        if (!empty($data)) {
            $response['data'] = $data;
        }

        return response()->json($response, $statusCode);
    }

    return response()->json($data, $statusCode);
}

/**
 * Convert array to 2d dot notation array.
 * 
 * @param array $arr
 * @return @return \Illuminate\Http\Response
 */
function dotArr(Array $arr) {
    $d_arr = [];
    [$params, $v] = Arr::divide(Arr::dot($arr));
    if (!empty($params)) {
        foreach($params as $param) {
            $col = Str::of($param)->explode('.');
            if (!empty($col)) {
                $d_key = NULL;
                foreach ($col as $c) {
                    if (!is_numeric($c)) {
                        if (isset($d_key)) {
                            $d_key .= '.' . $c;
                        } else {
                            $d_key = $c;
                        }
                    }
                }
                if (isset($d_key)) {
                    if (!in_array($d_key, $d_arr)) {
                        array_push($d_arr, $d_key);
                    }
                }
            }
        }
    }
    return $d_arr;
}