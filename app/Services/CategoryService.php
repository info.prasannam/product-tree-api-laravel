<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Category;
use App\Http\Resources\CategoryResource;

/**
 * @author [prasanna]
 * @email [info.prasannam@gmail.com]
 * @modify date 2022-03-14 13:13:53
 */

class CategoryService
{
    /**
     * Get the applicant list.
     * 
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id = null)
    {
        $category = Category::with(['children']);

        if (!is_null($id)) {
            $categories = new CategoryResource($category->findOrFail($id));
        } else {
            $categories = CategoryResource::collection($category->where('parent', 0)->get());
        }
        
        $responseData = new \stdClass;
        $responseData->request = $request;
        $responseData->statusCode = Response::HTTP_OK;
        $responseData->code = 40003;
        $responseData->data = $categories;

        return jsonResponse($responseData);
    }
}
