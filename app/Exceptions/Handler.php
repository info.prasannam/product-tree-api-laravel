<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

/**
 * @author [prasanna]
 * @email [info.prasannam@gmail.com]
 * @modify date 2022-03-14 13:13:53
 */

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        $responseData = new \stdClass;
        $responseData->request = $request;

        if ($exception instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
            $responseData->statusCode = Response::HTTP_UNAUTHORIZED;
            $responseData->code = 40002;
            return jsonErrorResponse($responseData);
        }

        if ($exception instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
            $responseData->statusCode = Response::HTTP_UNAUTHORIZED;
            $responseData->code = 40001;
            return jsonErrorResponse($responseData);
        }

        if ($exception instanceof \Tymon\JWTAuth\Exceptions\JWTException) {
            $responseData->statusCode = Response::HTTP_UNAUTHORIZED;
            $responseData->code = 40001;
            return jsonErrorResponse($responseData);
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            $responseData->statusCode = Response::HTTP_METHOD_NOT_ALLOWED;
            $responseData->data = ['statusCode' => Response::HTTP_METHOD_NOT_ALLOWED, 'error' => 'Method Not Allowed', 'message' => 'Request method is not supported'];
            return jsonErrorResponse($responseData);
        }

        if ($exception instanceof NotFoundHttpException) {
            $responseData->statusCode = Response::HTTP_NOT_FOUND;
            $responseData->data = ['statusCode' => Response::HTTP_NOT_FOUND, 'error' => 'Not Found', 'message' => 'Oh noes, there is nothing in here'];
            return jsonErrorResponse($responseData);
        }

        if ($exception instanceof ModelNotFoundException) {
            $responseData->statusCode = Response::HTTP_NOT_FOUND;
            $responseData->data = ['statusCode' => Response::HTTP_NOT_FOUND, 'error' => 'Not Found', 'message' => 'The records you are looking for could not be found'];
            return jsonErrorResponse($responseData);
        }

        if ($exception instanceof QueryException) {
            $responseData->statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
            $responseData->data = ['statusCode' => Response::HTTP_INTERNAL_SERVER_ERROR, 'error' => 'Server error', 'message' => 'Internal server error'];
            return jsonErrorResponse($responseData);
        }

        return parent::render($request, $exception);
    }
}
