<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Response;

/**
 * @author [prasanna]
 * @email [info.prasannam@gmail.com]
 * @modify date 2022-03-14 13:13:53
 */

class EnsureTokenIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $responseData = new \stdClass;
        $responseData->request = $request;
        $responseData->statusCode = Response::HTTP_UNAUTHORIZED;

        if(!$request->bearerToken()){
            $responseData->code = 40001;
            return jsonErrorResponse($responseData);
        }

        JWTAuth::setToken($request->bearerToken());

        $responseData->code = 40002;
        try {
            if (!JWTAuth::check()) {
                return jsonErrorResponse($responseData);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return jsonErrorResponse($responseData);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return jsonErrorResponse($responseData);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return jsonErrorResponse($responseData);
        }
        
        $request['auth_user'] = (object) JWTAuth::getPayload()->toArray();
        
        return $next($request);
    }
}
