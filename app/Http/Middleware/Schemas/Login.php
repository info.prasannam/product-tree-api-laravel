<?php

namespace App\Http\Middleware\Schemas;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;

/**
 * @author [prasanna]
 * @email [info.prasannam@gmail.com]
 * @modify date 2022-03-14 13:13:53
 */

class Login
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $responseData = new \stdClass;
        $responseData->request = $request;

        // Validate Payload
        $allowed_attributes = ['email', 'password'];

        $input_params = dotArr(Arr::except($request->all(), ['auth_user']));
        $invalid_params = array_diff($input_params, $allowed_attributes);

        if (!empty($invalid_params)) {
            $validation_error = (object) [head($invalid_params) => ['The ' . head($invalid_params) . ' is not allowed']];
            
            $responseData->statusCode = Response::HTTP_BAD_REQUEST;
            $responseData->data = ['statusCode' => Response::HTTP_BAD_REQUEST, 'error' => 'Bad Request', 'message' => 'Invalid Payload', 'validation' => $validation_error];

            return jsonErrorResponse($responseData);
        }

        // Validate Params
        $input = $request->all();

        $rules = [
            'email' => 'required|email',
            'password' => 'required'
        ];

        $messages = [];

        $attribute = [
            'email' => 'Email',
            'password' => 'Password'
        ];

        $validator = Validator::make($input, $rules, $messages, $attribute);

        if ($validator->fails()) {
            $responseData->statusCode = Response::HTTP_BAD_REQUEST;
            $responseData->data = ['statusCode' => Response::HTTP_BAD_REQUEST, 'error' => 'Bad Request', 'message' => 'Request validation failed', 'validation' => $validator->errors()];

            return jsonErrorResponse($responseData);
        }
        
        return $next($request);
    }
}
