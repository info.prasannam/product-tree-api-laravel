<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Response;

/**
 * @author [prasanna]
 * @email [info.prasannam@gmail.com]
 * @modify date 2022-03-14 13:13:53
 */

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Health Check
Route::get('health', function () {
    $responseData = new \stdClass;
    $responseData->statusCode = Response::HTTP_OK;
    $responseData->data = ['status' => 'healthy'];
    return jsonResponse($responseData);
});

//user login
Route::middleware(['user.login'])->post('login', 'AuthController@login');

//auth routes group
Route::group(['middleware' => ['auth.jwt']],function () {
    //category
    Route::get('category/{id?}', 'CategoryController@index');
});

/**
 * Redirect all unhandled routes
 */
Route::fallback(function () {
    $responseData = new \stdClass;
    $responseData->statusCode = Response::HTTP_NOT_FOUND;
    $responseData->data = ['statusCode' => Response::HTTP_NOT_FOUND, 'error' => 'Not Found', 'message' => 'Oh noes, there is nothing in here'];
    return jsonErrorResponse($responseData);
});