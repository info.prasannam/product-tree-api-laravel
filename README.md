# product-tree-api



## Getting started

## Installation

Initially clone the project from this repository.

```sh
https://gitlab.com/info.prasannam/product-tree-api-laravel.git
```

Please refer following steps to run the project in local or development
environment. First clone the project form this repository then run following
commands.


```sh
$ composer update
```

Please make a copy of the file `.env-dev` and name it` .env`. Then change the environment variables as you wish.


```sh
$ cp .env-dev .env
```

Then application will successfully run on your local environment.
